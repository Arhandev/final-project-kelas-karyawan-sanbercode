import { DownOutlined } from '@ant-design/icons';
import { Dropdown, Layout, Menu, message } from 'antd';
import axios from 'axios';
import React, { useContext } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { GlobalContext } from '../context/GlobalContext';
import { API_ENDPOINT } from '../utils/url';

const { Header, Footer } = Layout;

function BaseLayout({ children }) {
	const { loading, setLoading } = useContext(GlobalContext);
	const navigate = useNavigate();

	const onLogout = async values => {
		setLoading(true);
		try {
			const response = await axios.post(
				`${API_ENDPOINT}/final/logout`,
				{},
				{ headers: { Authorization: `Bearer ${localStorage.getItem('token')}` } }
			);
			localStorage.removeItem('token');
			localStorage.removeItem('username');
			navigate('/');
			message.success('Berhasil Logout');
		} catch (error) {
			message.error(error.response.data.info);
		} finally {
			setLoading(false);
		}
	};

	const menu = (
		<Menu
			items={[
				// {
				// 	key: 'cart',
				// 	label: <Link to="/carts">Carts</Link>,
				// },
				// {
				// 	key: 'transactions',
				// 	label: <Link to="/transactions">Transactions</Link>,
				// },
				{
					key: 'setting',
					label: <Link to="/setting">Setting</Link>,
				},
				{
					key: 'logout',
					label: <div onClick={onLogout}>Logout</div>,
					danger: true,
				},
			]}
		/>
	);

	return (
		<Layout className="min-h-screen">
			<Header className="grid grid-cols-12 items-center border-b overflow-hidden">
				<div className="col-span-2 justify-self-center">
					<img src="/images/logo.png" className="w-32" alt="" />
				</div>
				<Menu
					className="col-span-5"
					theme="dark"
					mode="horizontal"
					items={[
						{
							key: 'home',
							label: <Link to="/">Home</Link>,
						},
						{
							key: 'products',
							label: <Link to="/products">Products</Link>,
						},
					]}
				/>
				<div className="col-span-4 justify-self-end self-center flex items-center gap-4">
					{localStorage.getItem('token') === null ? (
						<>
							<Link to="/login">
								<button className="bg-primary text-white h-11 px-6 flex items-center justify-center rounded-xl border-2 border-primary text-lg font-bold">
									Login
								</button>
							</Link>
							<Link to="/register">
								<button className="bg-white text-primary h-11 px-6 flex items-center justify-center rounded-xl border-2 border-primary text-lg font-bold">
									Register
								</button>
							</Link>
						</>
					) : (
						<Dropdown overlay={menu} trigger={['click']}>
							<div className="flex gap-3 items-center text-primary text-xl">
								<p className="">Hi, {localStorage.getItem('username')}</p>
								<DownOutlined style={{ fontSize: 15 }} />
							</div>
						</Dropdown>
					)}
				</div>
			</Header>
			<div>{children}</div>
			{/* <Footer className="text-center">Copy Right Final Project React JS (Kelas Karyawan)</Footer> */}
		</Layout>
	);
}

export default BaseLayout;
