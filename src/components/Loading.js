import { LoadingOutlined } from '@ant-design/icons';
import { Spin } from 'antd';
import React from 'react';

const loadingIcon = <LoadingOutlined style={{ fontSize: 70 }} />;

function Loading() {
	return (
		<div className="fixed h-screen w-full bg-black bg-opacity-20 flex justify-center items-center z-20">
			<Spin indicator={loadingIcon} />
		</div>
	);
}

export default Loading;
