import { useContext } from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import './App.css';
import Loading from './components/Loading';
import { GlobalContext } from './context/GlobalContext';
import Cart from './pages/Cart';
import Checkout from './pages/Checkout';
import CreateProduct from './pages/CreateProduct';
import EditProduct from './pages/EditProduct';
import Home from './pages/Home';
import Login from './pages/Login';
import ProductDetail from './pages/ProductDetail';
import Products from './pages/Products';
import Register from './pages/Register';
import Setting from './pages/Setting';
import Transaction from './pages/Transaction';
import ProtectedRoute from './wrapper/ProtectedRoute';

const router = createBrowserRouter([
	{
		path: '/',
		element: <Home />,
	},
	{
		path: '/products',
		element: <Products />,
	},
	{
		path: '/login',
		element: <Login />,
	},
	{
		path: '/register',
		element: <Register />,
	},
	{
		path: '/products/:id',
		element: <ProductDetail />,
	},
	{
		element: <ProtectedRoute />,
		children: [
			{
				path: '/carts',
				element: <Cart />,
			},
			{
				path: '/checkout',
				element: <Checkout />,
			},
			{
				path: '/transactions',
				element: <Transaction />,
			},
			{
				path: '/setting',
				children: [
					{
						path: '',
						element: <Setting />,
					},
					{
						path: 'create',
						element: <CreateProduct />,
					},
					{
						path: ':id',
						element: <EditProduct />,
					},
				],
			},
		],
	},
]);

function App() {
	const { loading } = useContext(GlobalContext);

	return (
		<div className="App">
			{loading && <Loading />}
			<RouterProvider router={router} />
		</div>
	);
}

export default App;
