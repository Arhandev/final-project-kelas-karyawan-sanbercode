import { Input, message, Popconfirm, Select, Table } from "antd";
import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import BaseLayout from "../components/BaseLayout";
import { GlobalContext } from "../context/GlobalContext";
import { API_ENDPOINT } from "../utils/url";

const { Search } = Input;

function Setting() {
  const { setLoading } = useContext(GlobalContext);
  const [data, setData] = useState([]);
  const [search, setSearch] = useState("");
  const [filter, setFilter] = useState("");

  const [dataRaw, setDataRaw] = useState([]);
  const onDelete = async (id) => {
    setLoading(true);

    try {
      const response = await axios.delete(
        `${API_ENDPOINT}/final/products/${id}`,
        {
          headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
        }
      );
      fetchProducts();
      message.success("Berhasil menghapus barang");
    } catch (error) {
      message.error(error.response.data.info);
    } finally {
      setLoading(false);
    }
  };
  const columns = [
    {
      title: "Nama Barang",
      dataIndex: "nama",
      key: "nama",
    },
    {
      title: "Harga",
      dataIndex: "harga",
      key: "harga",
      render: (record, item) => `Rp. ${record}`,
    },
    {
      title: "Harga Diskon",
      dataIndex: "harga_diskon",
      key: "harga_diskon",
      render: (record, item) => (item.is_diskon ? `Rp. ${record}` : "-"),
    },
    {
      title: "Image",
      dataIndex: "image_url",
      key: "image_url",
      render: (record) => (
        <img src={record} className="w-32 h-32 object-cover" alt="gambar" />
      ),
    },
    {
      title: "Stock",
      dataIndex: "stock",
      key: "stock",
    },
    {
      title: "Category",
      dataIndex: "category",
      key: "category",
    },
    {
      title: "Dibuat Oleh",
      key: "pembuat",
      render: (record, item) => item.user.name,
    },
    {
      title: "Dibuat Pada",
      dataIndex: "created_at",
      key: "created_at",
    },
    {
      title: "Action",
      render: (record, item) => (
        <div className="flex items-center gap-4">
          <Link to={`/setting/${item.id}`}>
            <button className="py-2 px-4 border-2 border-yellow-400 bg-yellow-400 text-white font-bold rounded-lg">
              Edit
            </button>
          </Link>
          <Popconfirm
            title="Kamu yakin ingin menghapus barang ini?"
            onConfirm={() => {
              onDelete(item.id);
            }}
          >
            <button className="py-2 px-4 border-2 border-red-400 bg-red-400 text-white font-bold rounded-lg">
              Delete
            </button>
          </Popconfirm>
        </div>
      ),
    },
  ];

  const fetchProducts = async () => {
    setLoading(true);
    try {
      const response = await axios.get(`${API_ENDPOINT}/final/products`, {
        headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
      });
      setDataRaw(response.data.data);
      setData(response.data.data);
    } catch (error) {
      message.error(error.response.data.info);

      console.log(error);
    } finally {
      setLoading(false);
    }
  };
  useEffect(() => {
    fetchProducts();
  }, []);

  const handleChangeSelect = (value) => {
    setFilter(value);
    let allData = [...dataRaw];
    allData = allData.filter((data) => data.category === value);

    if (search !== "") {
      allData = allData.filter((data) =>
        data.nama.toLowerCase().includes(search.toLowerCase())
      );
    }
    setData(allData);
  };

  const onSearch = (value) => {
    setSearch(value);
    let allData = [...dataRaw];
    allData = allData.filter((data) =>
      data.nama.toLowerCase().includes(value.toLowerCase())
    );
    if (filter !== "") {
      allData = allData.filter((data) => data.category === filter);
    }
    setData(allData);
  };

  return (
    <BaseLayout>
      <div className="w-10/12 mx-auto" id="setting-product">
        <div className="flex justify-between items-center">
          <h1 className="text-4xl font-bold  mt-12 mb-4 ">Setting Products</h1>
          <Link to={"/setting/create"}>
            <button className="bg-primary text-white rounded-xl border-2 border-primary py-2 px-6 text-lg font-bold">
              Buat Product
            </button>
          </Link>
        </div>
        <div className="flex justify-end items-center gap-6">
          <div>
            <Select
              name="category"
              onChange={handleChangeSelect}
              value={filter}
            >
              <Select.Option value="" disabled>
                Filter Katgeori
              </Select.Option>
              <Select.Option value="teknologi">Teknologi</Select.Option>
              <Select.Option value="makanan">Makanan</Select.Option>
              <Select.Option value="minuman">minuman</Select.Option>
              <Select.Option value="lainnya">Lainnya</Select.Option>
            </Select>
          </div>
          <div>
            <Search
              placeholder="Search..."
              size="large"
              allowClear
              onSearch={onSearch}
            />
          </div>
        </div>
        <div className="mx-6 my-10">
          <Table columns={columns} dataSource={data} pagination={false} />
        </div>
      </div>
    </BaseLayout>
  );
}

export default Setting;
