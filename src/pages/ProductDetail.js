import { message } from 'antd';
import axios from 'axios';
import React, { useContext, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import BaseLayout from '../components/BaseLayout';
import { GlobalContext } from '../context/GlobalContext';
import { API_ENDPOINT } from '../utils/url';

function ProductDetail() {
	const { setLoading } = useContext(GlobalContext);
	const [product, setProduct] = useState([]);
	const navigate = useNavigate();

	const { id } = useParams();
	const fetchProduct = async () => {
		setLoading(true);
		try {
			const response = await axios.get(`${API_ENDPOINT}/final/products/${id}`);
			setProduct(response.data.data);
		} catch (error) {
			console.log(error);
			message.error(error.response.data.info);
		} finally {
			setLoading(false);
		}
	};
	useEffect(() => {
		fetchProduct();
	}, []);

	const addToCart = async () => {
		if (localStorage.getItem('token') === null) {
			navigate('/login');
		}
		setLoading(true);
		try {
			const response = await axios.post(
				`${API_ENDPOINT}/final/carts`,
				{ product_id: id },
				{ headers: { Authorization: `Bearer ${localStorage.getItem('token')}` } }
			);
			message.success('Berhasil Memasukkan barang ke keranjang');
			fetchProduct();
		} catch (error) {
			message.error(error.response.data.info);
			console.log(error);
		} finally {
			setLoading(false);
		}
	};

	return (
		<BaseLayout>
			<div className="w-10/12 mx-auto mt-24" style={{ height: '75vh' }}>
				<div className="grid grid-cols-12 gap-24 p-12  bg-white">
					<div className="col-span-5">
						<img src={product.image_url} className="w-full object-cover" style={{ height: 400 }} alt="" />
					</div>
					<div className="col-span-7 flex flex-col justify-around">
						<h1 className="text-4xl font-bold">{product.nama}</h1>
						{product.is_diskon === 1 ? (
							<div className="mt-6">
								<p className="text-blue-800 text-xl -mb-1 line-through">Rp {product.harga_display}</p>
								<p className="text-3xl text-red-500 font-bold">Rp {product.harga_diskon_display}</p>
							</div>
						) : (
							<p className="text-3xl text-blue-800 font-bold">Rp {product.harga_display}</p>
						)}
						<div className="text-xl">
							<span className="font-bold">Kategori:</span>
							<br /> {product.category}
						</div>
						<div className="text-xl">
							<span className="font-bold">Deskripsi:</span>
							<br /> {product.description}
						</div>
						<div className="">
							<p className="text-xl text-blue-500 mt-2 text-center">Stock {product.stock}</p>
						</div>
					</div>
				</div>
			</div>
		</BaseLayout>
	);
}

export default ProductDetail;
