import { Checkbox, Form, Input, message, Select } from 'antd';
import axios from 'axios';
import { Formik } from 'formik';
import React, { useContext, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import * as Yup from 'yup';
import BaseLayout from '../components/BaseLayout';
import { GlobalContext } from '../context/GlobalContext';
import { API_ENDPOINT } from '../utils/url';

const validationSchema = Yup.object({
	nama: Yup.string().required('Nama wajib diisi'),
	harga: Yup.number()
		.min(1000, 'Minimal harga adalah 1000')
		.typeError('Harap masukkan angka yang valid')
		.required('Harga wajib diisi'),
	stock: Yup.number()
		.min(1, 'Minimal stock adalah 1')
		.typeError('Harap masukkan stock yang valid')
		.required('stock wajib diisi'),
	is_diskon: Yup.boolean(),
	harga_diskon: Yup.number().when('is_diskon', {
		is: 1,
		then: Yup.number()
			.typeError('Harap masukkan angka yang valid')
			.min(1000, 'Minimal harga adalah 1000')
			.required('Harap Masukan harga diskon'),
	}),
	category: Yup.string().required('Kategori wajib diisi'),
	description: Yup.string().nullable(),
	image_url: Yup.string().url().required('Gambar wajib diisi'),
});

const initialState = {
	nama: '',
	harga: 0,
	harga_diskon: 0,
	is_diskon: false,
	image_url: '',
	description: '',
	stock: 0,
	category: '',
};

function EditProduct() {
	const { loading, setLoading } = useContext(GlobalContext);
	const { id } = useParams();
	const navigate = useNavigate();
	const [input, setInput] = useState(initialState);

	const onSubmit = async values => {
		setLoading(true);
		try {
			const response = await axios.put(
				`${API_ENDPOINT}/final/products/${id}`,
				{
					nama: values.nama,
					harga: values.harga,
					harga_diskon: values.harga_diskon,
					is_diskon: values.is_diskon,
					image_url: values.image_url,
					description: values.description,
					category: values.category,
					stock: values.stock,
				},
				{
					headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
				}
			);
			navigate('/setting');
			message.success('Berhasil Mengupdate Product');
		} catch (error) {
			message.error(error.response.data.info);
		} finally {
			setLoading(false);
		}
	};
	const fetchProduct = async () => {
		setLoading(true);
		try {
			const response = await axios.get(`${API_ENDPOINT}/final/products/${id}`);
			setInput({
				nama: response.data.data.nama,
				harga: response.data.data.harga,
				harga_diskon: response.data.data.harga_diskon,
				is_diskon: response.data.data.is_diskon,
				image_url: response.data.data.image_url,
				category: response.data.data.category,
				description: response.data.data.description,
				stock: response.data.data.stock,
			});
		} catch (error) {
			console.log(error);
			message.error(error.response.data.info);
		} finally {
			setLoading(false);
		}
	};
	useEffect(() => {
		fetchProduct();
	}, []);

	return (
		<BaseLayout>
			<div className="h-screen mt-16">
				<Formik initialValues={input} enableReinitialize validationSchema={validationSchema} onSubmit={onSubmit}>
					{({
						values,
						touched,
						handleChange,
						handleBlur,
						errors,
						handleSubmit,
						setFieldValue,
						setFieldTouched,
						isSubmitting,
					}) => (
						<div className="w-full max-w-lg mx-auto border-2 border-gray-400 rounded-xl p-10 bg-white">
							<Form className="" layout="vertical" onFinish={handleSubmit}>
								<h1 className="text-3xl font-bold mb-4">Edit Product Form</h1>
								<Form.Item
									label="Nama"
									help={touched.nama && errors.nama}
									validateStatus={touched.nama && errors.nama && 'error'}
									required
								>
									<Input
										placeholder="Masukan nama barang"
										onChange={handleChange}
										value={values.nama}
										onBlur={handleBlur}
										name="nama"
									/>
								</Form.Item>
								<Form.Item
									label="Harga"
									help={touched.harga && errors.harga}
									validateStatus={touched.harga && errors.harga && 'error'}
									required
								>
									<Input
										className="w-full"
										placeholder="Masukan harga barang"
										onChange={handleChange}
										value={values.harga}
										onBlur={handleBlur}
										name="harga"
									/>
								</Form.Item>
								<Form.Item
									label="Is Diskon"
									help={touched.harga_diskon && errors.harga_diskon}
									validateStatus={touched.harga_diskon && errors.harga_diskon && 'error'}
								>
									<Checkbox
										onChange={e => setFieldValue('is_diskon', e.target.checked)}
										value={values.is_diskon}
										checked={values.is_diskon}
									/>
								</Form.Item>
								{values.is_diskon == true && (
									<Form.Item
										label="Harga Diskon"
										help={touched.harga_diskon && errors.harga_diskon}
										validateStatus={touched.harga_diskon && errors.harga_diskon && 'error'}
										required
									>
										<Input
											className="w-full"
											placeholder="Masukan harga diskon barang"
											onChange={handleChange}
											value={values.harga_diskon}
											onBlur={handleBlur}
											name="harga_diskon"
										/>
									</Form.Item>
								)}
								<Form.Item
									label="Stock"
									help={touched.stock && errors.stock}
									validateStatus={touched.stock && errors.stock && 'error'}
									required
								>
									<Input
										className="w-full"
										placeholder="Masukan stock barang"
										onChange={handleChange}
										value={values.stock}
										onBlur={handleBlur}
										name="stock"
									/>
								</Form.Item>
								<Form.Item
									label="Deskripsi"
									help={touched.description && errors.description}
									validateStatus={touched.description && errors.description && 'error'}
								>
									<Input.TextArea
										placeholder="Masukan description barang"
										onChange={handleChange}
										value={values.description}
										onBlur={handleBlur}
										name="description"
									/>
								</Form.Item>
								<Form.Item
									label="Kategori Barang"
									help={touched.category && errors.category}
									validateStatus={touched.category && errors.category && 'error'}
									required
								>
									<Select
										defaultValue={values.category}
										value={values.category}
										onChange={value => {
											setFieldValue('category', value);
										}}
										onBlur={value => {
											setFieldTouched('category');
										}}
										name="category"
									>
										<Select.Option value="" disabled>
											Pilih Katgeori
										</Select.Option>
										<Select.Option value="teknologi">Teknologi</Select.Option>
										<Select.Option value="makanan">Makanan</Select.Option>
										<Select.Option value="minuman">minuman</Select.Option>
										<Select.Option value="lainnya">Lainnya</Select.Option>
									</Select>
								</Form.Item>
								<Form.Item
									label="Image URL"
									help={touched.image_url && errors.image_url}
									validateStatus={touched.image_url && errors.image_url && 'error'}
									required
								>
									<Input
										placeholder="Masukan link barang barang"
										onChange={handleChange}
										value={values.image_url}
										onBlur={handleBlur}
										name="image_url"
									/>
								</Form.Item>
								<div className="flex justify-center mt-12">
									<button
										type="submit"
										disabled={loading && isSubmitting}
										className="bg-primary py-2 px-10 rounded-lg text-white font-bold"
									>
										Simpan
									</button>
								</div>
							</Form>
						</div>
					)}
				</Formik>
			</div>
		</BaseLayout>
	);
}

export default EditProduct;
