import { Form, Input, message } from 'antd';
import axios from 'axios';
import { Formik } from 'formik';
import React, { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import BaseLayout from '../components/BaseLayout';
import { GlobalContext } from '../context/GlobalContext';
import { API_ENDPOINT } from '../utils/url';

const validationSchema = Yup.object({
	email: Yup.string().required('Email wajib diisi').email('Email tidak valid'),
	username: Yup.string().required('Username wajib diisi'),
	name: Yup.string().required('name wajib diisi'),
	password: Yup.string().required('Password wajib diisi'),
	password_confirmation: Yup.string().required('Konfirmasi Password wajib diisi'),
});

function Register() {
	const { loading, setLoading } = useContext(GlobalContext);
	const navigate = useNavigate();

	const initialState = {
		email: '',
		name: '',
		username: '',
		password: '',
		password_confirmation: '',
	};
	const onSubmit = async values => {
		setLoading(true);
		try {
			const response = await axios.post(`${API_ENDPOINT}/final/register`, {
				email: values.email,
				name: values.name,
				username: values.username,
				password: values.password,
				password_confirmation: values.password_confirmation,
			});
			navigate('/login');
			message.success('Berhasil Register. Silahkan Login');
		} catch (error) {
			message.error(error.response.data.info);
		} finally {
			setLoading(false);
		}
	};
	return (
		<BaseLayout>
			<div className="h-screen mt-16">
				<Formik initialValues={initialState} validationSchema={validationSchema} onSubmit={onSubmit}>
					{({ values, touched, handleChange, handleBlur, errors, handleSubmit, isSubmitting }) => (
						<div className="w-full max-w-lg mx-auto border-2 border-gray-400 rounded-xl p-10 bg-white">
							<Form className="" layout="vertical" onFinish={handleSubmit}>
								<h1 className="text-3xl font-bold mb-4">Register Form</h1>
								<Form.Item
									label="Nama"
									help={touched.name && errors.name}
									validateStatus={touched.name && errors.name && 'error'}
									required
								>
									<Input
										placeholder="Masukkan Nama"
										onChange={handleChange}
										onBlur={handleBlur}
										value={values.name}
										name="name"
									/>
								</Form.Item>
								<Form.Item
									label="Email"
									help={touched.email && errors.email}
									validateStatus={touched.email && errors.email && 'error'}
									required
								>
									<Input
										placeholder="Masukkan Email"
										onChange={handleChange}
										onBlur={handleBlur}
										value={values.email}
										name="email"
									/>
								</Form.Item>
								<Form.Item
									label="Username"
									help={touched.username && errors.username}
									validateStatus={touched.username && errors.username && 'error'}
									required
								>
									<Input
										placeholder="Masukkan Username"
										onChange={handleChange}
										onBlur={handleBlur}
										value={values.username}
										name="username"
									/>
								</Form.Item>

								<Form.Item
									label="Password"
									help={touched.password && errors.password}
									validateStatus={touched.password && errors.password && 'error'}
									required
								>
									<Input.Password
										placeholder="Masukkan Password"
										onChange={handleChange}
										onBlur={handleBlur}
										value={values.password}
										name="password"
									/>
								</Form.Item>
								<Form.Item
									label="Konfirmasi Password"
									help={touched.password_confirmation && errors.password_confirmation}
									validateStatus={touched.password_confirmation && errors.password_confirmation && 'error'}
									required
								>
									<Input.Password
										placeholder="Masukkan Password"
										onChange={handleChange}
										onBlur={handleBlur}
										value={values.password_confirmation}
										name="password_confirmation"
									/>
								</Form.Item>
								<div className="flex justify-center mt-12">
									<button
										disabled={loading && isSubmitting}
										type="submit"
										className="bg-primary py-2 px-10 rounded-lg text-white font-bold"
									>
										Sign up
									</button>
								</div>
							</Form>
						</div>
					)}
				</Formik>
			</div>
		</BaseLayout>
	);
}

export default Register;
