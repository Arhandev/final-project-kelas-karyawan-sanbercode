import { Card, message } from 'antd';
import axios from 'axios';
import React, { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import BaseLayout from '../components/BaseLayout';
import { GlobalContext } from '../context/GlobalContext';
import { API_ENDPOINT } from '../utils/url';

const { Meta } = Card;

function Cart() {
	const { setLoading } = useContext(GlobalContext);
	const [data, setData] = useState([]);

	const fetchCarts = async () => {
		setLoading(true);
		try {
			const response = await axios.get(`${API_ENDPOINT}/final/carts`, {
				headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
			});
			setData(response.data.data);
		} catch (error) {
			message.error('Terjadi suatu error');
			console.log(error);
		} finally {
			setLoading(false);
		}
	};
	useEffect(() => {
		fetchCarts();
	}, []);

	const onDelete = async id => {
		setLoading(true);
		try {
			const response = await axios.delete(`${API_ENDPOINT}/final/carts/${id}`, {
				headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
			});
			message.success('Berhasil menghapus produk dari keranjang');
			fetchCarts();
		} catch (error) {
			message.error('Terjadi suatu error');
			console.log(error);
		} finally {
			setLoading(false);
		}
	};
	return (
		<BaseLayout>
			<div className="w-10/12 mx-auto ">
				<div className="flex justify-between items-center mt-8 mb-4">
					<h1 className="text-4xl font-bold ">Keranjang</h1>
					{data.carts?.length > 0 ? (
						<Link to={'/checkout'}>
							<button className="bg-primary text-white rounded-xl border-2 border-primary py-2 px-6 text-lg font-bold">
								Checkout
							</button>
						</Link>
					) : (
						<button className="bg-gray-600 border-2 border-gray-600 rounded-xl text-gray-200 py-2 px-6 text-lg font-bold">
							Tidak Ada barang
						</button>
					)}
				</div>
				<div className="mx-6 grid grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 mt-10 gap-6">
					{data.carts?.map(item => (
						<Card
							hoverable
							className=""
							cover={<img src={item.product.image_url} className="w-full h-72 object-cover" alt="example" />}
						>
							<Meta
								description={
									<div className="flex flex-col gap-4 w-full">
										<Link to={`/products/${item.product.id}`}>
											<h1 className="text-xl font-bold truncate">{item.product.nama}</h1>
											{item.product.is_diskon === 1 ? (
												<div>
													<p className="text-blue-800 text-sm -mb-1 line-through">Rp {item.product.harga_display}</p>
													<p className="text-xl text-red-500 font-bold">Rp {item.product.harga_diskon_display}</p>
												</div>
											) : (
												<p className="text-xl text-blue-800 font-bold">Rp {item.product.harga_display}</p>
											)}
											<p className="text-lg text-blue-500">Stock {item.product.stock}</p>
										</Link>
									</div>
								}
							/>
							<button
								onClick={() => onDelete(item.id)}
								className="bg-red-400 border-2 border-red-400 text-white text-lg font-bold rounded-lg w-full mt-6"
							>
								Delete
							</button>
						</Card>
					))}
				</div>
			</div>
		</BaseLayout>
	);
}

export default Cart;
