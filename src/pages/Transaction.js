import { message, Table } from 'antd';
import axios from 'axios';
import React, { useContext, useEffect, useState } from 'react';
import BaseLayout from '../components/BaseLayout';
import { GlobalContext } from '../context/GlobalContext';
import { API_ENDPOINT } from '../utils/url';

const columns = [
	{
		title: 'Nama Penerima',
		dataIndex: 'nama_penerima',
		key: 'nama_penerima',
	},
	{
		title: 'Handphone',
		dataIndex: 'handphone',
		key: 'handphone',
	},
	{
		title: 'Alamat',
		dataIndex: 'alamat',
		key: 'alamat',
	},
	{
		title: 'Nama Penerima',
		dataIndex: 'nama_penerima',
		key: 'nama_penerima',
	},
	{
		title: 'Status',
		dataIndex: 'status',
		key: 'status',
	},
	{
		title: 'Total',
		dataIndex: 'total_display',
		key: 'total_display',
		render: record => `Rp. ${record}`,
	},
	{
		title: 'Dibuat Pada',
		dataIndex: 'created_at',
		key: 'created_at',
	},
];

function Transaction() {
	const { setLoading } = useContext(GlobalContext);
	const [data, setData] = useState([]);

	const fetchTransactions = async () => {
		setLoading(true);
		try {
			const response = await axios.get(`${API_ENDPOINT}/final/transactions`, {
				headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
			});
			setData(response.data.data);
		} catch (error) {
			message.error('Terjadi suatu error');
			console.log(error);
		} finally {
			setLoading(false);
		}
	};
	useEffect(() => {
		fetchTransactions();
	}, []);

	return (
		<BaseLayout>
			<div className="w-10/12 mx-auto ">
				<h1 className="text-4xl font-bold  mt-8 mb-4 ">List Transaksi</h1>
				<div className="mx-6 mt-10">
					<Table columns={columns} dataSource={data} pagination={false} />
				</div>
			</div>
		</BaseLayout>
	);
}

export default Transaction;
