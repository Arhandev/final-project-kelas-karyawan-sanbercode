import { Form, Input, message } from 'antd';
import axios from 'axios';
import { Formik } from 'formik';
import React, { useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import BaseLayout from '../components/BaseLayout';
import { GlobalContext } from '../context/GlobalContext';
import { API_ENDPOINT } from '../utils/url';

const validationSchema = Yup.object({
	nama_penerima: Yup.string().required('Nama penerima wajib diisi'),
	handphone: Yup.string().required('Handphone wajib diisi'),
	alamat: Yup.string().required('Alamat wajib diisi'),
});

function Checkout() {
	const { loading, setLoading } = useContext(GlobalContext);
	const navigate = useNavigate();
	const [data, setData] = useState([]);

	const initialState = {
		nama_penerima: '',
		handphone: '',
		alamat: '',
	};
	const fetchCarts = async () => {
		setLoading(true);
		try {
			const response = await axios.get(`${API_ENDPOINT}/final/carts`, {
				headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
			});
			setData(response.data.data);
		} catch (error) {
			message.error('Terjadi suatu error');
			console.log(error);
		} finally {
			setLoading(false);
		}
	};
	useEffect(() => {
		fetchCarts();
	}, []);

	const onSubmit = async values => {
		setLoading(true);
		try {
			const response = await axios.post(
				`${API_ENDPOINT}/final/transactions`,
				{
					nama_penerima: values.nama_penerima,
					handphone: values.handphone,
					alamat: values.alamat,
				},
				{ headers: { Authorization: `Bearer ${localStorage.getItem('token')}` } }
			);
			navigate('/transactions');
			message.success('Berhasil Melakukan Checkout');
		} catch (error) {
			message.error(error.response.data.info);
		} finally {
			setLoading(false);
		}
	};
	return (
		<BaseLayout>
			<div className="h-screen mt-16">
				<Formik initialValues={initialState} validationSchema={validationSchema} onSubmit={onSubmit}>
					{({ values, touched, handleChange, handleBlur, errors, handleSubmit }) => (
						<div className="w-full max-w-2xl mx-auto border-2 border-gray-400 rounded-xl p-10 bg-white">
							<Form className="" layout="vertical" onFinish={handleSubmit}>
								<h1 className="text-3xl font-bold mb-4">Checkout Form</h1>
								<Form.Item
									label="Nama Penerima"
									help={touched.nama_penerima && errors.nama_penerima}
									validateStatus={touched.nama_penerima && errors.nama_penerima && 'error'}
									required
								>
									<Input
										placeholder="Masukkan Nama Penerima"
										onChange={handleChange}
										onBlur={handleBlur}
										value={values.nama_penerima}
										name="nama_penerima"
									/>
								</Form.Item>
								<Form.Item
									label="Handphone"
									help={touched.handphone && errors.handphone}
									validateStatus={touched.handphone && errors.handphone && 'error'}
									required
								>
									<Input
										placeholder="Masukkan Handphone"
										onChange={handleChange}
										onBlur={handleBlur}
										value={values.handphone}
										name="handphone"
									/>
								</Form.Item>
								<Form.Item
									label="Alamat"
									help={touched.alamat && errors.alamat}
									validateStatus={touched.alamat && errors.alamat && 'error'}
									required
								>
									<Input.TextArea
										placeholder="Masukkan Alamat"
										onChange={handleChange}
										onBlur={handleBlur}
										value={values.alamat}
										name="alamat"
									/>
								</Form.Item>
								<div>
									<h1 className="text-xl font-bold">List Product</h1>
									<div className="mx-4 flex flex-col gap-6 mt-6">
										{data.carts?.map(cart => (
											<div key={cart.id} className="grid grid-cols-12 gap-6">
												<div className="col-span-4">
													<img src={cart.product.image_url} className="w-full h-32 object-cover" alt="" />
												</div>
												<div className="col-span-8">
													<h1 className="text-xl font-bold">{cart.product.nama}</h1>
													{cart.product.is_diskon === 1 ? (
														<div>
															<p className="text-blue-800 text-sm -mb-1 line-through">
																Rp {cart.product.harga_display}
															</p>
															<p className="text-xl text-red-500 font-bold">Rp {cart.product.harga_diskon_display}</p>
														</div>
													) : (
														<p className="text-xl text-blue-800 font-bold">Rp {cart.product.harga_display}</p>
													)}
												</div>
											</div>
										))}
										<div className="flex justify-between w-full font-bold text-xl">
											<h1>Total</h1>
											<h1>Rp. {data.total}</h1>
										</div>
									</div>
								</div>
								<div className="flex justify-center mt-12">
									<button type="submit" className="bg-primary py-2 px-10 rounded-lg text-white font-bold">
										Checkout
									</button>
								</div>
							</Form>
						</div>
					)}
				</Formik>
			</div>
		</BaseLayout>
	);
}

export default Checkout;
