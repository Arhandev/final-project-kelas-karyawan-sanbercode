import { Card, message } from 'antd';
import axios from 'axios';
import React, { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import BaseLayout from '../components/BaseLayout';
import { GlobalContext } from '../context/GlobalContext';
import { API_ENDPOINT } from '../utils/url';

const { Meta } = Card;

function Products() {
	const { setLoading } = useContext(GlobalContext);
	const [data, setData] = useState([]);

	const fetchProducts = async () => {
		setLoading(true);
		try {
			const response = await axios.get(`${API_ENDPOINT}/final/products`);
			setData(response.data.data);
		} catch (error) {
			message.error('Terjadi suatu error');
			console.log(error);
		} finally {
			setLoading(false);
		}
	};
	useEffect(() => {
		fetchProducts();
	}, []);

	return (
		<BaseLayout>
			<div className="my-12 w-10/12 mx-auto">
				<div className="flex justify-between">
					<h1 className="text-3xl font-bold">All Product</h1>
				</div>
				<div className="mx-6 grid grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 mt-6 gap-6">
					{data.map(item => (
						<Link to={`/products/${item.id}`}>
							<Card
								hoverable
								style={{ height: 450 }}
								className="overflow-hidden"
								cover={<img src={item.image_url} className="w-full h-72 object-cover" alt="example" />}
							>
								<Meta
									description={
										<div className="flex flex-col justify-between">
											<h1 className="text-2xl font-bold truncate">{item.nama}</h1>
											{item.is_diskon === 1 ? (
												<div>
													<p className="text-blue-800 text-sm -mb-1 line-through">Rp {item.harga_display}</p>
													<p className="text-xl text-red-500 font-bold">Rp {item.harga_diskon_display}</p>
												</div>
											) : (
												<p className="text-xl text-blue-800 font-bold">Rp {item.harga_display}</p>
											)}
											<p className="text-lg text-blue-500">Stock {item.stock}</p>
										</div>
									}
								/>
							</Card>
						</Link>
					))}
				</div>
			</div>
		</BaseLayout>
	);
}

export default Products;
